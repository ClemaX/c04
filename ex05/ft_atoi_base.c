/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_atoi_base.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/08 01:01:26 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 01:52:03 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

static unsigned int	ft_strlen(char *str)
{
	char *start;

	start = str;
	while (*str)
		str++;
	return (str - start);
}

static unsigned int	is_base_valid(char *base, int len)
{
	int i;
	int j;

	i = 0;
	if (len < 2)
		return (0);
	while (base[i++])
	{
		j = i + 1;
		if (base[i] == '+' || base[i] == '-')
			return (0);
		while (base[j++] && j <= len)
			if (base[i] == base[j])
				return (0);
	}
	return (1);
}

char				*sanitize(char *str)
{
	while ((*str >= '\t' && *str <= '\r') || *str == ' '
			|| *str == '-' || *str == '+')
		str++;
	return (str);
}

int					get_factor(char *str)
{
	int factor;

	factor = 1;
	while (*str == '+' || *str == '-')
	{
		if (*str == '-')
			factor *= -1;
		str++;
	}
	return (factor);
}

int					ft_atoi_base(char *str, char *base)
{
	const unsigned int	base_n = ft_strlen(base);
	int					nb;
	int					factor;
	unsigned int		j;

	if (!is_base_valid(base, base_n))
		return (0);
	nb = 0;
	factor = get_factor(str);
	str = sanitize(str);
	while (*str)
	{
		j = 0;
		while (j < base_n)
		{
			if (*str == base[j])
				nb = nb * base_n + j;
			j++;
		}
		str++;
	}
	return (nb * factor);
}
