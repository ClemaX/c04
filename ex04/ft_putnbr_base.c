/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnbr_base.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/07 20:59:14 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 00:54:18 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void				next_char(unsigned int nb, char *base, int base_n)
{
	char r;

	if (nb)
	{
		r = base[nb % base_n];
		next_char(nb / base_n, base, base_n);
		write(1, &r, 1);
	}
}

unsigned int		is_base_valid(char *base, int len)
{
	int i;
	int j;

	i = 0;
	if (len < 2)
		return (0);
	while (base[i++])
	{
		j = i + 1;
		if (base[i] == '+' || base[i] == '-')
			return (0);
		while (base[j++] && j <= len)
			if (base[i] == base[j])
				return (0);
	}
	return (1);
}

static unsigned int	ft_strlen(char *str)
{
	char *start;

	start = str;
	while (*str)
		str++;
	return (str - start);
}

void				ft_putnbr_base(int nbr, char *base)
{
	const unsigned int	base_n = ft_strlen(base);
	unsigned int		u_nb;

	if (!is_base_valid(base, base_n))
		return ;
	if (nbr < 0)
	{
		write(1, "-", 1);
		u_nb = nbr * -1;
	}
	else
		u_nb = nbr;
	if (u_nb <= base_n)
		write(1, &base[u_nb], 1);
	else
		next_char(u_nb, base, base_n);
}
