/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnbr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/06 15:55:57 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/07 15:25:09 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void	ft_nextchar(unsigned int nb)
{
	char r;

	if (nb)
	{
		r = nb % 10 + '0';
		nb = nb / 10;
		ft_nextchar(nb);
		write(1, &r, 1);
	}
}

void	ft_putnbr(int nb)
{
	unsigned int	u_nb;
	char			digit;

	u_nb = nb;
	if (nb < 0)
	{
		write(1, "-", 1);
		u_nb *= -1;
	}
	if (nb == 0)
		write(1, "0", 1);
	else if (u_nb < 10)
	{
		digit = u_nb + '0';
		write(1, &digit, 1);
	}
	else
		ft_nextchar(u_nb);
}
